package com.example.win.androidrealmfinalpoc;

import android.app.Application;

import io.realm.Realm;
import io.realm.RealmConfiguration;

/**
 * Created by Win on 30-10-2017.
 */

public class MyApplication extends Application{
    @Override
    public void onCreate() {
        super.onCreate();

        RealmConfiguration config = new RealmConfiguration.Builder(this).name("myrealm.realm").build();
        Realm.setDefaultConfiguration(config);

        
    }
}
