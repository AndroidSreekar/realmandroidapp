package com.example.win.androidrealmfinalpoc;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.win.androidrealmfinalpoc.Model.UserDetails;

import io.realm.RealmResults;

/**
 * Created by Win on 31-10-2017.
 */

public class ViewUserAdapter extends RecyclerView.Adapter<ViewUserAdapter.ViewUserHolder> {

    RealmResults<UserDetails> userDetails ;

    private Context context;
    public ViewUserAdapter(RealmResults<UserDetails> realmResults, Context applicationContext) {

        this.userDetails = realmResults;
        this.context = applicationContext;
    }

    @Override
    public ViewUserHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_inside_recycler_view,parent,false);
        return new ViewUserHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewUserHolder holder, int position) {

        UserDetails data = userDetails.get(position);
        holder.textViewUser.setText(data.getUserName());
        holder.textViewCountry.setText(data.getUserCountry());
        holder.textViewState.setText(data.getUserState());
        holder.textViewCapital.setText(data.getUserCapital());
    }

    @Override
    public int getItemCount() {
        return userDetails.size();
    }

    public class ViewUserHolder extends RecyclerView.ViewHolder{
        private TextView textViewUser;
        private TextView textViewCountry;
        private TextView textViewState;
        private TextView textViewCapital;

        public ViewUserHolder(View itemView) {
            super(itemView);
            textViewUser = (TextView) itemView.findViewById(R.id.textViewUserName);
            textViewCountry = (TextView) itemView.findViewById(R.id.textViewCountry);
            textViewState = (TextView) itemView.findViewById(R.id.textViewState);
            textViewCapital = (TextView) itemView.findViewById(R.id.textViewCapital);

            //its a comment
        }
    }
}
