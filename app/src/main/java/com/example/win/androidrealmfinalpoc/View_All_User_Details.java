package com.example.win.androidrealmfinalpoc;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.example.win.androidrealmfinalpoc.Model.UserDetails;

import io.realm.Realm;
import io.realm.RealmResults;

public class View_All_User_Details extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ViewUserAdapter Adapter;

    private Context context;
    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view__all__user__details);
        realm.getDefaultInstance();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_View);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        RealmResults<UserDetails> realmResults = realm.where(UserDetails.class)
                .findAllAsync();


        Adapter = new ViewUserAdapter(realmResults,getApplicationContext());
        recyclerView.setAdapter(Adapter);
    }
    public void recylerViewAction(){



    }
}
